package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Artist contains artist's information and details
type Artist struct {
	ID   primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	Name string             `json:"name" bson:"name"`
	Type string             `json:"type" bson:"type"`
}

// NewArtist creates a new Artist
func NewArtist() *Artist {
	artist := &Artist{}
	artist.ID = primitive.NewObjectID()

	return artist
}
