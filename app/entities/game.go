package entities

import (
	"app/api/socket"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	// JOINING indicates that the players are joining to the game
	JOINING = 0
	// STARTING indicates that the game is starting
	STARTING = 1
	// READY indicates that the players are ready
	READY = 2
	// STARTED indicates that the game has started
	STARTED = 3
	// ENDED indicates that the players has ended
	ENDED = 4
)

// Game contains game's information and details
type Game struct {
	ID        primitive.ObjectID       `json:"id,omitempty" bson:"_id,omitempty"`
	Date      time.Time                `json:"date,omitempty" bson:"date,omitempty"`
	Pending   map[*User]*socket.Client `json:"-" bson:"-"`
	Questions []*GameQuestion          `json:"questions,omitempty" bson:"questions,omitempty"`
	Full      bool                     `json:"full,omitempty" bson:"-"`
	Status    int                      `json:"status,omitempty" bson:"-"`
}

// GameQuestion contains game question information and details
type GameQuestion struct {
	ID           primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	QuestionID   primitive.ObjectID `json:"questionId,omitempty" bson:"question_id,omitempty"`
	Solution     int                `json:"-" bson:"solution"`
	Answers      []*UserAnswer      `json:"answers" bson:"answers"`
	Date         time.Time          `json:"date" bson:"date"`
	PendingUsers map[string]*User   `json:"-" bson:"-"`
}

// UserAnswer contains user answer details
type UserAnswer struct {
	UserID primitive.ObjectID `json:"userId" bson:"user_id"`
	Answer int                `json:"answer,omitempty" bson:"answer,omitempty"`
	Points int                `json:"points" bson:"points"`
	Life   int                `json:"life" bson:"life,omitempty"`
	Date   time.Time          `json:"date,omitempty" bson:"date,omitempty"`
}

// NewGame creates a new game
func NewGame() *Game {
	game := &Game{}
	game.ID = primitive.NewObjectID()
	game.Date = time.Now()

	return game
}

// IsReady checks if a game is ready
func (g *Game) IsReady() bool {
	return len(g.Pending) == 0
}
