package entities

import (
	"app/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// User contains user's information and details
type User struct {
	ID       primitive.ObjectID       `json:"id" bson:"_id,omitempty"`
	Username string                   `json:"username,omitempty" bson:"username,omitempty"`
	Password string                   `json:"-" bson:"password,omitempty"`
	Avatar   string                   `json:"avatar,omitempty" bson:"avatar,omitempty"`
	Facebook *models.FacebookResponse `json:"-" bson:"facebook,omitempty"`
	Google   *models.GoogleResponse   `json:"-" bson:"google,omitempty"`
	Token    string                   `json:"token,omitempty" bson:"token,omitempty"`
}

// Response returns user with Google and Facebook to nil
func (user User) Response() User {
	user.Google = nil
	user.Facebook = nil

	return user
}

// Info returns user info
func (user User) Info() User {
	user.Token = ""
	return user.Response()
}

// NewUser creates a new user
func NewUser() *User {
	user := &User{}
	user.ID = primitive.NewObjectID()
	user.Avatar = "noavatar.jpg"

	return user
}
