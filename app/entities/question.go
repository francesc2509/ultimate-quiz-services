package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Question contains question's information and details
type Question struct {
	ID       primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Solution int                `json:"-" bson:"solution,omitempty"`
	Text     *I18nText          `json:"text,omitempty" bson:"text,omitempty"`
	Options  []*QuestionOption  `json:"options,omitempty" bson:"options,omitempty"`
}

// QuestionOption contains the schema of a question option
type QuestionOption struct {
	Text *I18nText `json:"text" bson:"text"`
	Key  int32     `json:"key,omitempty" bson:"key,omitempty"`
}

// I18nText contains the schema of a multilanguage text
type I18nText struct {
	Default string `json:"default" bson:"default"`
	En      string `json:"en" bson:"en"`
	Es      string `json:"es" bson:"es"`
}
