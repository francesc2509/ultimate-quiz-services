package img

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// DownloadImg downloads an image from the specified Url
// and save it into the provided path.
func DownloadImg(prefix string, url string, extension string) (string, error) {
	res, err := getImgData(url)

	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	date := time.Now().UTC().UnixNano()
	//open a file for writing
	filename := fmt.Sprint(prefix, date, extension)
	filepath := fmt.Sprint("img/", filename)
	file, err := os.Create(fmt.Sprint("app/api/public/", filepath))
	if err != nil {
		return "", err
	}
	defer file.Close()

	// Use io.Copy to just dump the response body to the file. This supports huge files
	_, err = io.Copy(file, res.Body)
	if err != nil {
		return "", err
	}
	return filepath, nil
}

// getImgData gets http response from provided url
func getImgData(url string) (*http.Response, error) {
	res, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	statusCode := res.StatusCode
	if statusCode > 299 && statusCode < 400 {
		return getImgData(res.Header["location"][0])
	}
	return res, err
}
