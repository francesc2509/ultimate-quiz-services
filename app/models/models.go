package models

// TokenRequest is the model of an OAuth's token request
type TokenRequest struct {
	AccessToken string `json:"token"`
}

// GoogleResponse google login schema
type GoogleResponse struct {
	Email         string `json:"email"`
	FamilyName    string `json:"family_name"`
	GivenName     string `json:"given_name"`
	ID            string `json:"id"`
	Locale        string `json:"locale"`
	Name          string `json:"name"`
	Picture       string `json:"picture"`
	VerifiedEmail bool   `json:"verified_email"`
}

// FacebookResponse facebook login schema
type FacebookResponse struct {
	Email   string     `json:"email"`
	ID      string     `json:"id"`
	Name    string     `json:"name"`
	Picture *FBPicture `json:"picture"`
	// Picture string `json:"picture"`
	// "picture": Object {
	//   "data": Object {
	//     "height": 50,
	//     "is_silhouette": true,
	//     "url": "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=440568230083756&height=50&width=50&ext=1556216503&hash=AeRWdxLQo3W9sl2F",
	//     "width": 50,
	//   },
	// },
}

// FBPicture facebook's avatar schema
type FBPicture struct {
	Info *FBPictureDetails `json:"data"`
}

// FBPictureDetails facebook's avatar details schema
type FBPictureDetails struct {
	Height       int    `json:"height"`
	IsSilhouette bool   `json:"is_silhouette"`
	URL          string `json:"url"`
	Width        int    `json:"width"`
}
