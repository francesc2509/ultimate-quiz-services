package mongodb

import (
	"context"
	"log"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoDB ...
type MongoDB struct {
	client *mongo.Client
}

func (db *MongoDB) connect() *mongo.Client {
	if db.client != nil {
		return db.client
	}
	// Set client options
	clientOptions := options.Client().ApplyURI(dbURL)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	// err = client.Ping(context.TODO(), nil)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	log.Println("Connected to MongoDB!")
	db.client = client
	return db.client
}

func (db *MongoDB) disconnect(client *mongo.Client) {
	// // Close the connection once no longer needed
	// err := client.Disconnect(context.TODO())

	// if err != nil {
	// 	log.Fatal(err)
	// } else {
	// 	log.Println("Connection to MongoDB closed.")
	// }
}

// Select gets a slice of documents
func (db *MongoDB) Select(name string, params interface{}, t reflect.Type) ([]interface{}, error) {
	client := db.connect()

	collection := client.Database(dbName).Collection(name)
	ctx, fn := context.WithTimeout(context.Background(), 30*time.Second)
	defer fn()

	cursor, err := collection.Find(ctx, bson.D{})

	if err != nil {
		return nil, err
	}
	result, err := iterateCursor(ctx, cursor, t)

	db.disconnect(client)
	return result, err
}

// Insert creates a new document
func (db *MongoDB) Insert(name string, data interface{}) error {
	client := db.connect()
	collection := client.Database(dbName).Collection(name)
	result, err := collection.InsertOne(context.Background(), data)
	db.disconnect(client)
	if err != nil {
		return err
	}

	err = db.FindByID(result.InsertedID.(primitive.ObjectID), name, &data)

	if err != nil {
		return err
	}

	return nil
}

// FindOne returns one register that matches the filter
func (db *MongoDB) FindOne(address interface{}, params map[string]interface{}, name string) error {
	client := db.connect()

	collection := client.Database(dbName).Collection(name)
	result := collection.FindOne(context.Background(), params)

	err := result.Err()
	if err != nil {
		return err
	}

	return result.Decode(address)
}

// FindByID returns the document identified by the specified id
func (db *MongoDB) FindByID(id primitive.ObjectID, name string, address interface{}) error {
	client := db.connect()
	defer db.disconnect(client)

	collection := client.Database(dbName).Collection(name)
	err := collection.FindOne(
		context.Background(),
		map[string]interface{}{"_id": id},
	).Decode(address)

	if err != nil {
		return err
	}

	return nil
}

// GetRandom returns randomly obtained documents
func (db *MongoDB) GetRandom(
	name string, params interface{}, t reflect.Type,
) ([]interface{}, error) {
	client := db.connect()
	ctx, fn := context.WithTimeout(context.Background(), 30*time.Second)

	defer fn()

	collection := client.Database(dbName).Collection(name)
	// [{$sample: {size: 1}}]
	cursor, err := collection.Aggregate(ctx, []map[string]interface{}{
		{
			"$sample": map[string]interface{}{
				"size": 2,
			},
		},
	})

	if err != nil {
		return nil, err
	}

	result, err := iterateCursor(ctx, cursor, t)
	db.disconnect(client)

	return result, err
}

// Update updates one register that matches the filter
func (db *MongoDB) Update(name string, params interface{}, filter interface{}) error {
	client := db.connect()
	ctx, fn := context.WithTimeout(context.Background(), 30*time.Second)
	defer fn()
	collection := client.Database(dbName).Collection(name)
	_, err := collection.UpdateOne(ctx, filter, params)

	return err
}

// iterateCursor iterates a MongoDB's cursor
func iterateCursor(ctx context.Context, cursor *mongo.Cursor, t reflect.Type) ([]interface{}, error) {
	defer cursor.Close(ctx)
	var result []interface{}
	for cursor.Next(ctx) {
		item := reflect.New(t.Elem()).Interface()

		err := cursor.Decode(item)
		if err != nil {
			return nil, err
		}
		result = append(result, item)
	}

	return result, nil
}
