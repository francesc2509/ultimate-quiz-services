package db

import (
	"app/core/db/mongodb"
	"reflect"
)

// Engine ...
type Engine interface {
	FindOne(address interface{}, params map[string]interface{}, name string) error
	GetRandom(name string, params interface{}, t reflect.Type) ([]interface{}, error)
	Insert(name string, data interface{}) error
	Select(name string, params interface{}, t reflect.Type) ([]interface{}, error)
	Update(name string, params interface{}, filter interface{}) error
}

// DB ...
var db Engine = &mongodb.MongoDB{}

// DB eturns db's engine
func DB() Engine {
	return db
}
