package artist

import (
	"app/core/db"
	"app/entities"
	"reflect"
)

var Repository = &repository{}

// Repository ...
type repository struct{}

func (r *repository) Get() ([]interface{}, error) {
	return db.DB().Select("artists", nil, reflect.TypeOf(&entities.Artist{}))
}

func (r *repository) Create(artist interface{}) error {
	return db.DB().Insert("artists", artist)
}
