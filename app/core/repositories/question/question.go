package question

import (
	"app/core/db"
	"app/entities"
	"reflect"
)

// Repository ...
var Repository = &repository{}

type repository struct{}

// Get returns a slice of questions
func (r *repository) Get() ([]interface{}, error) {
	// db.artists.aggregate([{$sample: {size: 1}}])
	return db.DB().GetRandom("questions", nil, reflect.TypeOf(&entities.Question{}))
}
