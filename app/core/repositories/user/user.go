package user

import (
	"app/core/db"
	"app/entities"
)

// Repository ...
var Repository = &repository{}

type repository struct{}

// Create creates a new user
func (r *repository) Create(user interface{}) error {
	return db.DB().Insert("users", user)
}

// Update updates an user and returns it
func (r *repository) Update(params map[string]interface{}, filter map[string]interface{}) (interface{}, error) {
	err := db.DB().Update("users", params, filter)

	if err != nil {
		return nil, err
	}

	user := &entities.User{}
	err = db.DB().FindOne(user, filter, "users")

	return user, err
}

// FindOne searches a user and returns it
func (r *repository) FindOne(params map[string]interface{}) (interface{}, error) {
	user := &entities.User{}
	err := db.DB().FindOne(user, params, "users")

	return user, err
}
