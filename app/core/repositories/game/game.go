package game

import (
	"app/core/db"
)

const (
	collectionName = "games"
)

// Repository ...
var Repository = &repository{}

type repository struct{}

// Create a new game
func (r *repository) Create(game interface{}) error {
	return db.DB().Insert(collectionName, game)
}

// Update a game
func (r *repository) Update(params interface{}, filter interface{}) error {
	return db.DB().Update(collectionName, params, filter)
}
