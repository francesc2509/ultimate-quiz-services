package repositories

import (
	"app/core/repositories/artist"
	"app/core/repositories/game"
	"app/core/repositories/question"
	"app/core/repositories/user"
)

// Artist Repository
var Artist = artist.Repository

// Game Repository
var Game = game.Repository

// Question Repository
var Question = question.Repository

// User Repository
var User = user.Repository
