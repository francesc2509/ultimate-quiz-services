package artist

import (
	"app/api/graphql/types"
	"app/api/services"
	"app/entities"

	"github.com/graphql-go/graphql"
)

// HandleQuery adds the graphql fields corresponding to
// artist queries
func HandleQuery(fields *graphql.Fields) {
	f := (*fields)
	f["artistGet"] = get()
}

// HandleMutation adds the graphql fields corresponding to
// artist mutations
func HandleMutation(fields *graphql.Fields) {
	f := (*fields)
	f["artistCreate"] = create()
}

func get() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ArtistType),
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return services.Artist.Get()
		},
	}
}

func create() *graphql.Field {
	return &graphql.Field{
		Type: types.ArtistType,
		Args: graphql.FieldConfigArgument{
			"name": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"type": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			artist := entities.NewArtist()
			artist.Type = params.Args["type"].(string)
			artist.Name = params.Args["name"].(string)
			return services.Artist.Create(artist)
		},
	}
}
