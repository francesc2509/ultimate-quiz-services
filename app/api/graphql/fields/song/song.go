package song

import (
	"app/api/graphql/types"
	"app/api/services"

	"github.com/graphql-go/graphql"
)

// HandleQuery adds the graphql fields corresponding to
// songs queries
func HandleQuery(fields *graphql.Fields) {
	(*fields)["songs"] = getSongs()
}

func getSongs() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.SongType),
		Args: graphql.FieldConfigArgument{
			"album": &graphql.ArgumentConfig{
				Type:         graphql.String,
				DefaultValue: "",
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return services.Song.Get(params.Args["album"].(string))
		},
	}
}
