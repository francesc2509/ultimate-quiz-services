package fields

import (
	"app/api/graphql/fields/artist"
	"app/api/graphql/fields/question"
	"app/api/graphql/fields/song"
	"app/api/graphql/fields/user"

	"github.com/graphql-go/graphql"
)

// HandleQuery returns the queries graphql fields added
func HandleQuery() graphql.Fields {
	queryFields := make(graphql.Fields)

	song.HandleQuery(&queryFields)
	artist.HandleQuery(&queryFields)
	question.HandleQuery(&queryFields)

	return queryFields
}

// HandleMutation returns the mutations graphql fields added
func HandleMutation() graphql.Fields {
	mutationFields := make(graphql.Fields)

	artist.HandleMutation(&mutationFields)
	user.HandleMutation(&mutationFields)

	return mutationFields
}
