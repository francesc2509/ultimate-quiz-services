package user

import (
	"app/api/graphql/types"
	"app/api/services"
	"app/entities"
	"errors"

	"github.com/graphql-go/graphql"
)

// HandleMutation adds the graphql fields corresponding to
// user mutations
func HandleMutation(fields *graphql.Fields) {
	f := (*fields)
	f["userEdit"] = edit()
}

func edit() *graphql.Field {
	return &graphql.Field{
		Type: types.UserType,
		Args: graphql.FieldConfigArgument{
			"username": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			ctx := params.Context

			user, ok := ctx.Value(services.Auth.ParamKey()).(*entities.User)

			if !ok || user == nil {
				return nil, errors.New("Cannot get user")
			}

			username, ok := params.Args["username"].(string)
			if !ok {
				return nil, errors.New("Invalid username")
			}
			return services.User.EditUsername(username, user)
		},
	}
}
