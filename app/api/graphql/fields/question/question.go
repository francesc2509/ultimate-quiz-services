package question

import (
	"app/api/graphql/types"
	"app/api/services"

	"github.com/graphql-go/graphql"
)

// HandleQuery adds the graphql fields corresponding to
// question queries
func HandleQuery(fields *graphql.Fields) {
	(*fields)["questionGet"] = getQuestions()
}

func getQuestions() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.QuestionType),
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			return services.Question.Get()
		},
	}
}
