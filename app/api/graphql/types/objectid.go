package types

import (
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// NewObjectID creates a new ObjectID from an hex-string
// and returns it
func NewObjectID(v string) *primitive.ObjectID {
	value, err := primitive.ObjectIDFromHex(v)

	if err != nil {
		return nil
	}

	return &value
}

// ObjectIDType is the graphql schema associated to ObjectID
var ObjectIDType = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "ObjectId",
	Description: "The `ObjectIdType` scalar type represents an ObjectId.",
	// Serialize serializes `CustomID` to string.
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case primitive.ObjectID:
			return value.Hex()
		case *primitive.ObjectID:
			v := *value
			return v.Hex()
		default:
			return nil
		}
	},
	// ParseValue parses GraphQL variables from `string` to `primitive.ObjectId`.
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			return NewObjectID(value)
		case *string:
			return NewObjectID(*value)
		default:
			return nil
		}
	},
	// ParseLiteral parses GraphQL AST value to `primitive.ObjectId`.
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return NewObjectID(valueAST.Value)
		default:
			return nil
		}
	},
})
