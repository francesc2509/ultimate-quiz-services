package types

import "github.com/graphql-go/graphql"

// UserType stores GraphQL's type configuration associated to model user
var UserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: ObjectIDType,
		},
		"avatar": &graphql.Field{
			Type: graphql.String,
		},
		"username": &graphql.Field{
			Type: graphql.String,
		},
	},
})
