package types

import "github.com/graphql-go/graphql"

// ArtistType stores GraphQL's type configuration associated to model Artist
var ArtistType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Artist",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: ObjectIDType,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"type": &graphql.Field{
			Type: graphql.String,
		},
	},
})
