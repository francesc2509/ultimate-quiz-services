package types

import "github.com/graphql-go/graphql"

// QuestionType stores GraphQL's type configuration associated to model Question
var QuestionType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Question",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: ObjectIDType,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"type": &graphql.Field{
			Type: graphql.String,
		},
	},
})
