package auth

import (
	"app/api/rest/writer"
	"app/api/services/user"
	"app/entities"
	"app/models"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type jwtClaims struct {
	PK   string
	Name string
	*jwt.StandardClaims
}

// Service exports an instance of authentification service
var Service = &service{}

type service struct{}

type key int

const (
	paramKey key = iota
)

var secretKey []byte

func (s *service) CreateToken(user *entities.User) (string, error) {
	claims := &jwtClaims{
		user.ID.Hex(),
		user.Username,
		&jwt.StandardClaims{
			Id:        "main_user_id",
			ExpiresAt: time.Now().Add(720 * time.Hour).Unix(),
		},
	}

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	err := tokenSecret()
	if err != nil {
		return "", err
	}
	return rawToken.SignedString(secretKey)
}

func (s *service) IsAuthorized(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := r.URL.Query()
		reqToken := ""

		if token := params.Get("access_token"); token != "" {
			reqToken = token
		} else if r.Header["Authorization"] != nil {
			reqToken = strings.Replace(r.Header["Authorization"][0], "Bearer ", "", -1)
		}

		if reqToken != "" {
			token, err := jwt.Parse(reqToken, tokenCallback)

			if err == nil && token.Valid {
				claims := parseToken(token)
				if token.Claims != nil {
					u, err := user.Service.GetByID(claims["PK"].(string))
					if err != nil {
						writer.SendNotFound(w, nil)
						return
					}
					token, err := s.CreateToken(u)

					if err != nil {
						writer.SendError(w, err.Error(), http.StatusInternalServerError)
						return
					}

					u.Token = token
					r = storeUserInCtx(r, u)

					handler(w, r)
					return
				}
			}
		}
		writer.SendUnauthorized(w, nil)
	}
}

func (s *service) Google(w http.ResponseWriter, r *http.Request) (*entities.User, error) {
	tokenRequest, err := decodeTokenLoginPayload(r)

	if err != nil {
		return nil, err
	}
	url := "https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + tokenRequest.AccessToken
	res := &models.GoogleResponse{}
	err = requestWithToken(w, url, res)

	if err != nil {
		return nil, err
	}

	user, err := user.Service.CreateGoogleUser(res)

	if err != nil {
		return nil, err
	}
	err = s.setUserToken(user)
	return user, err
}

func (s *service) Facebook(w http.ResponseWriter, r *http.Request) (*entities.User, error) {
	tokenRequest, err := decodeTokenLoginPayload(r)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf(
		"https://graph.facebook.com/me?access_token=%s&fields=id,name,email,about,picture",
		tokenRequest.AccessToken,
	)

	res := &models.FacebookResponse{}
	err = requestWithToken(w, url, res)
	if err != nil {
		return nil, err
	}

	user, err := user.Service.CreateFBUser(res)

	if err != nil {
		return nil, err
	}
	err = s.setUserToken(user)
	return user, err
}

func (s *service) ParamKey() key {
	return paramKey
}

func (s *service) setUserToken(user *entities.User) error {
	token, err := s.CreateToken(user)
	if err != nil {
		return err
	}
	user.Token = token

	return nil
}

func parseToken(token *jwt.Token) jwt.MapClaims {
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		return claims
	}
	return nil
}

func storeUserInCtx(r *http.Request, user *entities.User) *http.Request {
	ctx := context.WithValue(r.Context(), paramKey, user)
	r = r.WithContext(ctx)

	return r
}

func tokenSecret() error {
	if secretKey == nil {
		b, err := ioutil.ReadFile("keys/json-web-token-secret.txt") // just pass the file name
		if err != nil {
			return err
		}
		secretKey = b
	}
	return nil // print the content as 'bytes'
}

func decodeTokenLoginPayload(r *http.Request) (*models.TokenRequest, error) {
	tokenRequest := &models.TokenRequest{}
	payload := r.Body
	defer payload.Close()
	err := json.NewDecoder(payload).Decode(&tokenRequest)

	return tokenRequest, err
}

func requestWithToken(w http.ResponseWriter, url string, address interface{}) error {
	response, err := http.Get(url)

	if err != nil {
		writer.SendUnauthorized(w, nil)
		return err
	}

	data, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	if err != nil {
		return err
	}
	err = json.Unmarshal(data, address)

	if err != nil {
		return err
	}

	return nil
}

func tokenCallback(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("There was an error")
	}

	err := tokenSecret()
	if err != nil {
		return "", err
	}
	return secretKey, nil
}
