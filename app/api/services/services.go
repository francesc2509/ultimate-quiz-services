package services

import (
	"app/api/services/artist"
	"app/api/services/auth"
	"app/api/services/game"
	"app/api/services/question"
	"app/api/services/song"
	"app/api/services/user"
)

// Artist is the service that manages artist's entity
var Artist = artist.Service

// Song is the service that manages song's entity
var Song = song.Service

// Auth is the service that manages authentification's related stuff
var Auth = auth.Service

// Question is the service that manages question's entity
var Question = question.Service

// Game is the service that manages
var Game = game.Service

// User is the service that manages
var User = user.Service
