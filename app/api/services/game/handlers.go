package game

import (
	"app/api/services/question"
	"app/api/socket"
	"app/entities"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"
)

// createResponse creates a response that match socket's schema
func createResponse(key string, data interface{}, user *entities.User) ([]byte, error) {
	var u *entities.User

	if user != nil {
		usr := user.Info()
		u = &usr
	}

	res := &socket.Packet{
		Key: key,
		Payload: &socket.Payload{
			Date: time.Now(),
			Message: map[string]interface{}{
				"user": u,
				"data": data,
			},
		},
	}

	return json.Marshal(res)
}

// initHandlers initializes socket handlers
func initHandlers(h *socket.Hub) {
	h.HandleRegister = register
	h.HandleUnregister = unregister
	h.AddHandler("user_ready", userReadyHandler)
	h.AddHandler("user_unready", userNotReadyHandler)
	h.AddHandler("question_res", questionResHandler)
}

func getPlayer(client *socket.Client) *player {
	return players[client]
}

func getUserGame(player *player) (*entities.Game, *entities.User) {
	if player == nil {
		return nil, nil
	}
	return player.game, player.user
}

func getLife(client *socket.Client) int {
	item := players[client]
	if item == nil {
		return -1
	}
	return item.life
}

// register adds a client to hub
func register(client *socket.Client) error {
	player := getPlayer(client)
	game, user := getUserGame(player)
	h := client.Hub()
	rooms := h.Rooms()
	id := game.ID.Hex()
	room := rooms[id]

	if room == nil {
		room = socket.NewRoom()
		h.AddRoom(id, room)
	}

	users := func() []entities.User {
		arr := []entities.User{}
		for _, c := range room.Clients() {
			arr = append(arr, players[c].user.Info())
		}

		return arr
	}()
	room.AddClient(client)

	var message []byte
	var err error

	full := room.Capacity() <= len(room.Clients())
	message, err = createResponse(
		"user_joined",
		map[string]interface{}{
			"users": users,
			"full":  full,
		},
		user,
	)
	if err != nil {
		return err
	}

	var begin func()
	if full {
		begin = func() { initGame(h, room, game) }
	}
	go h.PrepareAndSend(id, message, begin, 2)

	return nil
}

// unregister removes a client from hub
func unregister(client *socket.Client) error {
	log.Println("Unregister")
	if client.Status() == socket.ClientDisconnected {
		return nil
	}

	player := getPlayer(client)
	game, user := getUserGame(player)

	id := game.ID.Hex()
	hub := client.Hub()
	room := hub.Rooms()[id]

	if ok := room.RemoveClient(client); ok {

		client.Disconnect()
		delete(players, client)
		delete(game.Pending, user)
		clients := room.Clients()
		clientCount := len(clients)

		fmt.Printf("Client count: %d\n", clientCount)

		if clientCount <= 1 {

			if clientCount < 1 {
				room.Close()
				delete(hub.Rooms(), id)
				return nil
			}

			if game.Status == entities.STARTING {
				room.Close()
			}
		}

		if game.Status == entities.JOINING {
			message, err := createResponse("user_left", nil, user)
			if err != nil {
				return err
			}

			go hub.PrepareAndSend(id, message, nil, 0)
		}
	} else {
		fmt.Println("Players")
		fmt.Println(players)
		fmt.Println("Cannot delete")
	}
	return nil
}

// userReadyHandler set player state to ready
func userReadyHandler(client *socket.Client, p *socket.Payload) error {
	player := getPlayer(client)
	game, user := getUserGame(player)
	id := game.ID.Hex()
	hub := client.Hub()
	room := hub.Rooms()[id]
	delete(game.Pending, user)
	client.SetStatus(socket.ClientReady)

	if game.IsReady() {
		room.Timer().Stop()
		err := startGame(hub, game)
		return err
	}

	return nil
}

// userNotReadyHandler set player state to NotReady
func userNotReadyHandler(client *socket.Client, p *socket.Payload) error {
	player := getPlayer(client)
	game, user := getUserGame(player)
	game.Pending[user] = client
	client.SetStatus(socket.ClientNoReady)

	game.Status = entities.STARTING

	return nil
}

// initGame prepares the game and setup the beginning of it
func initGame(hub *socket.Hub, room *socket.Room, game *entities.Game) {
	if len(room.Clients()) < 2 {
		return
	}

	id := game.ID.Hex()
	game.Pending = getPending(room)
	game.Status = entities.STARTING
	game.Full = true
	m, err := createResponse("user_join_complete", "Starting game...", nil)
	if err != nil {
		return
	}

	start := func() { startGame(hub, game) }
	go hub.PrepareAndSend(id, m, start, 60)
}

// startGame starts the game
func startGame(hub *socket.Hub, game *entities.Game) error {
	game, err := Service.Create(game)

	if err != nil {
		return err
	}

	m, err := createResponse(
		"game_start",
		map[string]int{"life": 100},
		nil,
	)
	if err != nil {
		return err
	}

	send := func() {
		game.Status = entities.STARTED
		sendQuestion(hub, game)
	}

	hub.PrepareAndSend(game.ID.Hex(), m, send, 5)

	return nil
}

// sendQuestion sends a question to the players
func sendQuestion(hub *socket.Hub, game *entities.Game) error {
	if game.Status == entities.ENDED {
		return nil
	}

	id := game.ID.Hex()
	questions, err := question.Service.Get()

	if err != nil {
		return err
	}

	question := questions[0]
	m, err := createResponse("question_req", question, nil)
	if err != nil {
		return err
	}

	_, err = Service.AddQuestion(game, question)
	if err != nil {
		return err
	}

	room := hub.Rooms()[id]
	game.Pending = getPending(room)

	callback := func() { expireQuestion(hub, game) }
	go hub.PrepareAndSend(id, m, callback, 10)

	return nil
}

// expireQuestion notify players that the question expired
func expireQuestion(hub *socket.Hub, game *entities.Game) {
	id := game.ID.Hex()
	room := hub.Rooms()[id]
	clients := room.Clients()

	if game.Status == entities.ENDED {
		return
	}

	m, err := createResponse("question_expired", nil, nil)
	if err != nil {
		return
	}

	callback := func() { sendQuestion(hub, game) }
	delay := 5

	if won, c := isThereWinner(clients); won {
		callback = func() { Service.finishGame(c) }
		delay = 0
	}
	go hub.PrepareAndSend(id, m, callback, delay)
}

// questionResHandler handles a player response
func questionResHandler(c *socket.Client, p *socket.Payload) error {
	player := getPlayer(c)
	game, user := getUserGame(player)

	if !isPlayerPending(player, game) {
		return nil
	}

	life := getLife(c)
	key, ok := p.Message.(float64)

	if !ok {
		return errors.New("Bad Response")
	}

	a, err := Service.AddUserAnswer(int(key), game, user, life)
	if err != nil {
		return err
	}
	delete(game.Pending, user)

	return managePoints(c, a.Points)
}

// managePoints manages the points obtained by a player
func managePoints(c *socket.Client, points int) error {
	hub := c.Hub()
	player := getPlayer(c)
	game := player.game
	id := game.ID.Hex()
	clients := hub.Rooms()[id].Clients()

	var callback func()
	if len(game.Pending) == 0 {
		callback = func() {
			if game.Status == entities.ENDED {
				return
			}

			room := hub.Rooms()[id]
			room.Timer().Stop()
			sendQuestion(hub, game)
		}
	}

	correctAnswer := false
	if points > 0 {
		correctAnswer = true
		attack := healPlayer(player, points)

		if attack > 0 {
			sendAttack(game, attack)
			if won, client := isThereWinner(clients); won {
				Service.finishGame(client)
			}
		}
	}

	message := map[string]interface{}{
		"result": correctAnswer,
		"life":   player.life,
	}

	m, err := createResponse("attack_sent", message, nil)
	if err != nil {
		go c.Unregister()
		return err
	}

	go c.SendMessage(m, callback, 1)
	return nil
}

// healPlayer restores player health points
func healPlayer(player *player, points int) int {
	max := 1000
	maxLife := 100
	life := player.life

	pObtained := int((float32(points) / float32(max)) * 100)
	lifeDiff := maxLife - life

	fmt.Printf("Life: %d, LifeDiff: %d, Obtained: %d\n", life, lifeDiff, pObtained)

	if lifeDiff > 0 {
		if pObtained > lifeDiff {
			life += lifeDiff
			pObtained -= lifeDiff
		} else {
			life += pObtained
			pObtained = 0
		}
		player.life = life
	}

	return pObtained
}

// sendAttack sends an attack to the pending players
func sendAttack(game *entities.Game, totalPoints int) error {

	pending := game.Pending
	if len(pending) <= 0 {
		return nil
	}

	count := len(pending)
	attack := int(totalPoints / count)

	ko := false
	koCount := 0

	for _, client := range pending {
		player := getPlayer(client)
		if player.life <= attack {
			attack = player.life
			ko = true
			delete(pending, player.user)
			koCount++
		}
		player.life -= attack

		message := map[string]interface{}{
			"attack": attack,
			"ko":     ko,
		}
		m, err := createResponse("attack_received", message, nil)
		if err != nil {
			go client.Unregister()
			return err
		}
		go client.SendMessage(m, nil, 0)
	}

	return nil
}

// getPending gets the players pending
func getPending(room *socket.Room) map[*entities.User]*socket.Client {
	m := make(map[*entities.User]*socket.Client)
	for _, client := range room.Clients() {
		player := getPlayer(client)
		if player != nil && player.life > 0 {
			user := player.user
			m[user] = client
		}
	}

	return m
}

// isThereWinner checks if there is already a game winner
func isThereWinner(clients []*socket.Client) (bool, *socket.Client) {
	alive := 0
	var client *socket.Client

	for _, c := range clients {
		player := getPlayer(c)
		if player.life > 0 {
			alive++
			if alive > 1 {
				return false, nil
			}
			client = c
		}
	}

	return true, client
}

func isPlayerPending(player *player, game *entities.Game) bool {
	for user := range game.Pending {
		if user == player.user {
			return true
		}
	}
	return false
}
