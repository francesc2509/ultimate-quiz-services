package game

import (
	"app/api/rest/writer"
	"app/api/services/auth"
	"app/api/socket"
	"app/core/repositories"
	"app/entities"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type service struct {
	hub         *socket.Hub
	current     *entities.Game
	activeGames map[string]*entities.Game
}

type player struct {
	user *entities.User
	game *entities.Game
	life int
}

// Service ...
var Service = &service{}

var players = make(map[*socket.Client]*player)

// RunSocket creates the socket and runs it
func (s *service) RunSocket() http.HandlerFunc {
	s.hub = socket.NewHub()
	hub := s.hub
	initHandlers(hub)

	go hub.Run()
	return s.ServeWs
}

// Current gets the last public game
func (s *service) Current() (*entities.Game, error) {
	if s.current == nil {
		s.current = entities.NewGame()
	} else if s.current.Full {
		if s.activeGames == nil {
			s.activeGames = make(map[string]*entities.Game)
		}
		s.activeGames[s.current.ID.Hex()] = s.current
		s.current = entities.NewGame()
	}

	return s.current, nil
}

// Create a new game
func (s *service) Create(game *entities.Game) (*entities.Game, error) {
	err := repositories.Game.Create(*game)

	return game, err
}

// AddQuestion adds a question to a game
func (s *service) AddQuestion(game *entities.Game, question *entities.Question) (*entities.GameQuestion, error) {
	q := &entities.GameQuestion{
		ID:         primitive.NewObjectID(),
		Answers:    []*entities.UserAnswer{},
		QuestionID: question.ID,
		Date:       time.Now(),
		Solution:   question.Solution,
	}
	game.Questions = append(game.Questions, q)

	filter := map[string]interface{}{"_id": game.ID}

	params := make(map[string]interface{})
	params["$push"] = map[string]interface{}{"questions": q}

	return q, repositories.Game.Update(params, filter)
}

// AddUserAnswer adds a user answer to a game question
func (s *service) AddUserAnswer(
	answer int,
	game *entities.Game,
	user *entities.User,
	life int,
) (*entities.UserAnswer, error) {
	questions := game.Questions
	last := questions[len(questions)-1]

	a := &entities.UserAnswer{
		Date:   time.Now(),
		UserID: user.ID,
		Answer: answer,
		Life:   life,
		Points: 0,
	}

	if a.Answer == last.Solution {
		duration := a.Date.Sub(last.Date)
		a.Points = int(1000 - (duration.Seconds() * 100))

		// fmt.Println(a.Points)
	}

	filter := map[string]interface{}{
		"_id":           game.ID,
		"questions._id": last.ID,
	}

	params := make(map[string]interface{})
	params["$push"] = map[string]interface{}{"questions.$.answers": a}

	err := repositories.Game.Update(params, filter)

	return a, err
}

// ServeWs handles websocket requests from the peer.
func (s *service) ServeWs(w http.ResponseWriter, r *http.Request) {
	hub := s.hub
	game, err := s.Current()
	if err != nil {
		log.Println(err)
		writer.SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user, ok := r.Context().Value(auth.Service.ParamKey()).(*entities.User)
	if !ok {
		writer.SendError(w, "Something bad happened", http.StatusInternalServerError)
		return
	}

	// for client, player := range players {
	// 	id := player.user.ID.Hex()

	// 	if id == user.ID.Hex() {
	// 		go client.Unregister()
	// 	}
	// }
	log.Println("New client connected")

	client := socket.NewClient()
	players[client] = &player{
		game: game,
		user: user,
		life: 100,
	}

	err = client.Connect(w, r)
	if err != nil {
		delete(players, client)
		log.Println(err)
		writer.SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	client.SetHub(hub)
	go client.WritePump()
	go client.ReadPump()
}

// finishGame ends a game
func (s *service) finishGame(c *socket.Client) error {
	hub := c.Hub()
	player := getPlayer(c)
	game := player.game
	id := game.ID.Hex()

	m, err := createResponse(
		"game_ended",
		nil,
		player.user,
	)

	if err != nil {
		return err
	}

	room := hub.Rooms()[id]
	game.Status = entities.ENDED
	room.Timer().Stop()

	time.AfterFunc(1*time.Second, func() { hub.PrepareAndSend(id, m, nil, 0) })
	delete(s.activeGames, id)
	return nil
}
