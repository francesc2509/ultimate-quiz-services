package artist

import (
	"app/core/repositories"
	"app/entities"
)

// Service ...
var Service = &service{}

type service struct{}

// Get a slice of artists
func (service *service) Get() ([]*entities.Artist, error) {
	result, err := repositories.Artist.Get()

	if err != nil {
		return nil, err
	}

	var artists []*entities.Artist
	for _, item := range result {
		artists = append(artists, item.(*entities.Artist))
	}

	return artists, nil
}

// Create an artist
func (service *service) Create(artist *entities.Artist) (*entities.Artist, error) {
	err := repositories.Artist.Create(*artist)

	return artist, err
}
