package question

import (
	"app/core/repositories"
	"app/entities"
)

// Service ...
var Service = &service{}

type service struct{}

// Get returns a slice of questions
func (s *service) Get() ([]*entities.Question, error) {
	result, err := repositories.Question.Get()

	if err != nil {
		return nil, err
	}

	var questions []*entities.Question
	for _, item := range result {
		questions = append(questions, item.(*entities.Question))
	}

	return questions, nil
}
