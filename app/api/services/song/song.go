package song

import (
	"app/entities"
	"strings"
)

var songs = []*entities.Song{
	&entities.Song{
		ID:       "1",
		Album:    "ts-fearless",
		Title:    "Fearless",
		Duration: "4:01",
		Type:     "song",
	},
	&entities.Song{
		ID:       "2",
		Album:    "ts-fearless",
		Title:    "Fifteen",
		Duration: "4:54",
		Type:     "song",
	},
}

// Service ...
var Service = &service{}

type service struct{}

func (service *service) Get(album string) ([]*entities.Song, error) {
	if album == "" {
		return songs, nil
	}

	filtered := filter(songs, func(v *entities.Song) bool {
		return strings.Contains(v.Album, album)
	})
	return filtered, nil
	// return songs, nil
}

func filter(songs []*entities.Song, f func(*entities.Song) bool) []*entities.Song {
	vsf := make([]*entities.Song, 0)
	for _, v := range songs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}
