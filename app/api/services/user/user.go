package user

import (
	"app/core/repositories"
	"app/entities"
	"app/models"
	"app/utils/img"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service ...
var Service = &service{}

type service struct{}

func (service *service) CreateGoogleUser(res *models.GoogleResponse) (*entities.User, error) {
	u, _ := repositories.User.FindOne(map[string]interface{}{
		"google.email": res.Email,
	})

	user, ok := u.(*entities.User)
	if !ok {
		return nil, errors.New("Row is not a user")
	}

	if user.Google == nil {
		user = entities.NewUser()
		user.Username = res.Name
		user.Google = res

		createAvatar(user, res.Picture)

		err := repositories.User.Create(user)
		if err != nil {
			return nil, err
		}
	}

	return user, nil
}

func (service *service) CreateFBUser(res *models.FacebookResponse) (*entities.User, error) {
	u, _ := repositories.User.FindOne(map[string]interface{}{
		"facebook.email": res.Email,
	})

	user, ok := u.(*entities.User)
	if !ok {
		return nil, errors.New("Row is not a user")
	}

	if user.Facebook == nil {
		user = entities.NewUser()
		user.Username = res.Name
		user.Facebook = res

		createAvatar(user, res.Picture.Info.URL)

		err := repositories.User.Create(user)
		if err != nil {
			return nil, err
		}
	}

	return user, nil
}

func (service *service) GetByID(id string) (*entities.User, error) {
	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, err
	}
	user, err := repositories.User.FindOne(map[string]interface{}{
		"_id": oid,
	})

	if err != nil {
		return nil, err
	}

	u, ok := user.(*entities.User)
	if !ok {
		return nil, errors.New("Not a user")
	}
	return u, nil
}

func (service *service) EditUsername(username string, user *entities.User) (*entities.User, error) {
	filter := map[string]interface{}{"_id": user.ID}

	params := make(map[string]interface{})
	params["$set"] = map[string]interface{}{"username": username}

	result, err := repositories.User.Update(params, filter)

	if err != nil {
		return nil, err
	}

	if address, ok := result.(*entities.User); ok {
		user := address.Info()
		fmt.Println(user)
		return &user, err
	}
	return nil, errors.New("Internal Server Error")
}

func createAvatar(user *entities.User, url string) {
	filename, err := img.DownloadImg(user.ID.Hex(), url, ".jpg")
	if err == nil {
		user.Avatar = filename
	}
}
