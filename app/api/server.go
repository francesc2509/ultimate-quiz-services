package api

import (
	"app/api/graphql/schema"
	"app/api/middlewares"
	"app/api/rest"
	"app/api/rest/writer"
	"fmt"
	"log"
	"net/http"
	"servergo"
)

// StartServer starts a new Http server
func StartServer(host string, port int) {
	fmt.Printf("Server running on %s:%d...\n", host, port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), setUpRouter()))
}

func setUpRouter() http.Handler {
	router := servergo.New()
	router.NotFoundHandler = writer.SendNotFound
	router.UnauthorizedHandler = writer.SendUnauthorized
	router.Use(middlewares.Log)

	// Auth
	rest.Auth.Init(router)

	// GraphQL
	graphql := router.AddGroup("/graphql")
	graphql.HandleFunc("", schema.Init(), middlewares.Auth).Methods("GET", "POST")

	// WebSocket
	// router.HandleFunc("/echo", socket.Socket.Handler)
	rest.Game.Init(router)

	fileHandler := http.FileServer(http.Dir("app/api/public"))
	router.HandleFileFunc("/", fileHandler.ServeHTTP)

	return router
}
