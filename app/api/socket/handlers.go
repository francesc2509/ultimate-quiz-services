package socket

import "fmt"

type handler func(c *Client, p *Payload) error

var handlers = make(map[string]handler)

func execHandler(c *Client, p *Packet) {
	if h, ok := handlers[p.Key]; ok {
		err := h(c, p.Payload)
		if err != nil {
			fmt.Println(err)
		}
	}
}
