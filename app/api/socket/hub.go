// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package socket

import (
	"log"
	"time"
)

type broadcastPayload struct {
	ID            string
	payload       []byte
	callback      func()
	callbackDelay int
}

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	rooms map[string]*Room

	// Inbound messages from the clients.
	broadcast chan *broadcastPayload

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	// handlers
	handlers map[string]handler

	// Register
	HandleRegister func(c *Client) error

	// Register
	HandleUnregister func(c *Client) error
}

// NewHub creates a new instance of hub
func NewHub() *Hub {
	return &Hub{
		broadcast:  make(chan *broadcastPayload),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		rooms:      make(map[string]*Room),
		handlers:   make(map[string]handler),
	}
}

// Rooms returns hub's rooms
func (h *Hub) Rooms() map[string]*Room {
	return h.rooms
}

// AddRoom adds a new room to the hub
func (h *Hub) AddRoom(id string, room *Room) {
	h.rooms[id] = room
}

// AddHandler adds a new handler to the hub
func (h *Hub) AddHandler(key string, handler handler) {
	handlers[key] = handler
}

// Run starts hub's connection
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			err := h.HandleRegister(client)
			if err != nil {
				log.Printf("error: %v", err)
			}
		case client := <-h.unregister:
			err := h.HandleUnregister(client)
			if err != nil {
				log.Printf("error: %v", err)
			}
		case data := <-h.broadcast:
			room := h.rooms[data.ID]
			if room == nil {
				break
			}

			clients := room.clients

			if clients == nil || len(clients) < 1 {
				break
			}

			for _, client := range clients {
				select {
				case client.send <- &sendPayload{data: data.payload}:
				default:
					if room.RemoveClient(client) {
						client.Unregister()
					}
				}
			}

			callback := data.callback
			if callback == nil {
				break
			}

			delay := time.Duration(data.callbackDelay)
			if delay == 0 {
				callback()
				break
			}
			room.SetTimer(time.AfterFunc(time.Second*delay, callback))
		}
	}
}

// PrepareAndSend prepares the data and broadcast it
func (h *Hub) PrepareAndSend(roomID string, message []byte, callback func(), delay int) {
	data := &broadcastPayload{roomID, message, callback, delay}

	go h.sendBroadcast(data)
}

// SendBroadcast sends data to hub's broadcast address
func (h *Hub) sendBroadcast(data *broadcastPayload) {
	h.broadcast <- data
}

func (h *Hub) isReady(id string) bool {
	clients := h.rooms[id].clients
	for _, client := range clients {
		if client.status == ClientNoReady {
			return false
		}
	}
	return len(clients) > 0
}
