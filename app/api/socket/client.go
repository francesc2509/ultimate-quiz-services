// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package socket

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

const (
	// ClientNoReady status specifies that the client is not ready to play
	ClientNoReady = 0
	// ClientReady status specifies that the client is ready to play
	ClientReady = 1
	// ClientPending status specifies that the client is pending to respond
	ClientPending = 2
	// ClientDone status specifies that the client has already responded
	ClientDone = 3
	// ClientDisconnected status specifies that the client is disconnected
	ClientDisconnected = 4
)

type sendPayload struct {
	data          []byte
	callback      func()
	callbackDelay int
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *sendPayload

	// Informs client's status
	status int

	ticker *time.Ticker
}

// NewClient creates a new Client
func NewClient() *Client {
	return &Client{
		send: make(chan *sendPayload),
	}
}

// Connect creates a connection between the client and the socket
func (c *Client) Connect(w http.ResponseWriter, r *http.Request) error {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return err
	}
	c.conn = conn

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	// go c.WritePump()
	// go c.ReadPump()
	return nil
}

// Hub returns the client's hub
func (c *Client) Hub() *Hub {
	return c.hub
}

// SetHub sets the client's hub
func (c *Client) SetHub(hub *Hub) {
	c.hub = hub
	c.hub.register <- c
}

// Status returns the client's status
func (c *Client) Status() int {
	return c.status
}

// SetStatus sets the client's status
func (c *Client) SetStatus(status int) {
	c.status = status
}

// // Send return client's send channel
// func (c *Client) Send() chan []byte {
// 	return c.send
// }

// SendMessage sends the message to the client
func (c *Client) SendMessage(message []byte, callback func(), delay int) {
	c.send <- &sendPayload{
		data:          message,
		callback:      callback,
		callbackDelay: delay,
	}
}

// ReadPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) ReadPump() {
	defer func() {
		log.Println("ReadPump")
		// c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
				c.hub.unregister <- c
			}
			break
		}

		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		res := &Packet{}
		json.Unmarshal(message, res)

		if res.Payload == nil {
			res.Payload = &Payload{}
		}
		res.Payload.Client = c
		res.Payload.Date = time.Now()
		execHandler(c, res)
	}
}

// WritePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) WritePump() {
	c.ticker = time.NewTicker(pingPeriod)
	defer func() {
		log.Println("WritePump")
		c.ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case result, ok := <-c.send:

			if !ok {
				fmt.Println("Hola")
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			message := result.data
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				fmt.Println("dadasdas 2")
				return
			}
			w.Write(message)

			// // Add queued chat messages to the current websocket message.
			// n := len(c.send)
			// for i := 0; i < n; i++ {
			// 	w.Write(newline)
			// 	w.Write((<-c.send).data)
			// }

			if err := w.Close(); err != nil {
				fmt.Println("dadasdas 3")
				return
			}

			if callback := result.callback; callback != nil {
				delay := time.Duration(result.callbackDelay)
				time.AfterFunc(time.Second*delay, callback)
			}
		case <-c.ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// Unregister calls client's hub unregister handler
func (c *Client) Unregister() {
	c.hub.unregister <- c
}

// Disconnect close the client connection with the socket
func (c *Client) Disconnect() {
	log.Println("Disconnect!")
	close(c.send)
	c.SetStatus(ClientDisconnected)
	c.ticker.Stop()
	c.conn.Close()
}
