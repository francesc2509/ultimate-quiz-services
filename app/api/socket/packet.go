package socket

import (
	"time"
)

// Packet is the schema that the response must match
type Packet struct {
	Key     string   `json:"name"`
	Payload *Payload `json:"payload,omitempty"`
}

// Payload is the schema that the sent data must match
type Payload struct {
	Date    time.Time   `json:"date,omitempty"`
	Message interface{} `json:"message,omitempty"`
	Client  *Client     `json:"-"`
}
