package socket

import (
	"time"
)

// Room stores a list of clients
type Room struct {
	clients  []*Client
	capacity int
	timer    *time.Timer
}

// NewRoom creates a new room
func NewRoom() *Room {
	return &Room{
		clients:  []*Client{},
		capacity: 3,
	}
}

// Capacity returns room's capacity
func (room *Room) Capacity() int {
	return room.capacity
}

// Clients returns room's clients
func (room *Room) Clients() []*Client {
	return room.clients
}

// Timer returns room's timer
func (room *Room) Timer() *time.Timer {
	return room.timer
}

// SetTimer sets room's timer
func (room *Room) SetTimer(timer *time.Timer) {
	room.timer = timer
}

// AddClient adds a client to the room
func (room *Room) AddClient(client *Client) {
	room.clients = append(room.clients, client)
}

// RemoveClient removes a client to the room
func (room *Room) RemoveClient(client *Client) bool {
	clients := room.clients

	for i, c := range clients {
		if c == client {
			if len(clients) == 1 {
				room.clients = []*Client{}
			} else {
				limit := len(clients)
				clients[limit-1], clients[i] = clients[i], clients[limit-1]
				room.clients = clients[:limit-1]
			}
			return true
		}
	}
	return false
}

// Full returns if the room is full
func (room *Room) Full() bool {
	return len(room.clients) >= room.capacity
}

// Close closes the room
func (room *Room) Close() {
	if room.timer != nil {
		room.timer.Stop()
	}

	for _, client := range room.clients {
		go client.Unregister()
	}
}
