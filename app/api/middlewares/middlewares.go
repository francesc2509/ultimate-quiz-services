package middlewares

import (
	"app/api/services"
	"log"
	"net/http"
	"servergo"
)

// Auth middleware secures a Route or a Group of routes
var Auth servergo.Middleware = func(next http.HandlerFunc) http.HandlerFunc {
	return services.Auth.IsAuthorized(next)
}

// Log middleware logs request info
var Log servergo.Middleware = func(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL.String())

		next(w, r)
	}
}
