package auth

import (
	"app/api/middlewares"
	"app/api/rest/writer"
	"app/api/services"
	"app/entities"
	"fmt"
	"net/http"
	"os"
	"servergo"

	"golang.org/x/oauth2/google"

	"golang.org/x/oauth2"
)

var (
	googleOAuthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8080/auth/googlE",
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}
)

// Controller exports AuthController
var Controller = &authController{}

type authController struct{}

// Init adds auth routes to the provided router
func (controller *authController) Init(router *servergo.Router) {
	group := router.AddGroup("/auth")
	group.HandleFunc("/google", handleGoogleOAuth).Methods("POST")
	group.HandleFunc("/facebook", handleFacebookOAuth).Methods("POST")
	group.HandleFunc("/validate", validate, middlewares.Auth).Methods("POST")
}

func handleGoogleOAuth(w http.ResponseWriter, r *http.Request) {
	user, err := services.Auth.Google(w, r)

	if err != nil {
		writer.SendNotFound(w, nil)
		return
	}

	result := (*user).Response()
	writer.SendResponseOK(w, result, http.StatusOK)
}

func handleFacebookOAuth(w http.ResponseWriter, r *http.Request) {
	user, err := services.Auth.Facebook(w, r)
	if err != nil {
		writer.SendNotFound(w, nil)
		return
	}

	result := (*user).Response()
	writer.SendResponseOK(w, result, http.StatusOK)
}

func validate(w http.ResponseWriter, r *http.Request) {
	user, ok := r.Context().Value(services.Auth.ParamKey()).(*entities.User)
	if !ok {
		fmt.Println("dasdasd")
		writer.SendNotFound(w, nil)
		return
	}

	result := (*user).Response()
	writer.SendResponseOK(w, result, http.StatusOK)
}
