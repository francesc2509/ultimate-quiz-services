package game

import (
	"app/api/middlewares"
	"app/api/services"
	"servergo"
)

type gameController struct{}

// Controller exports an instance of Game Controller
var Controller = &gameController{}

// Init adds auth routes to the provided router
func (controller *gameController) Init(router *servergo.Router) {
	handler := services.Game.RunSocket()

	group := router.AddGroup("/game", middlewares.Auth)
	group.HandleFunc("/ws", handler).Methods("GET")
}
