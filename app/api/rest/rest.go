package rest

import (
	"app/api/rest/auth"
	"app/api/rest/game"
)

// Auth exports AuthController
var Auth = auth.Controller

// Game exports GameController
var Game = game.Controller
