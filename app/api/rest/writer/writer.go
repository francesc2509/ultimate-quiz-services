package writer

import (
	"encoding/json"
	"net/http"
)

type responseError struct {
	Message interface{} `json:"message"`
}

type response struct {
	Ok   bool           `json:"ok"`
	Data interface{}    `json:"data"`
	Err  *responseError `json:"error"`
}

// SendResponseOK sends an successful response with the provided http response code
func SendResponseOK(w http.ResponseWriter, data interface{}, code int) {
	if code > 299 {
		panic("Status code is not valid")
	}

	w.WriteHeader(code)
	send(w, data, true, nil)
}

// SendError Sends an error response with the provided http response code
func SendError(w http.ResponseWriter, message interface{}, code int) {
	if code < 299 {
		panic("Status code is not valid")
	}

	w.WriteHeader(code)
	send(w, nil, false, &responseError{message})
}

// SendNotFound sends an error response with status not found
func SendNotFound(w http.ResponseWriter, message interface{}) {
	if message == nil {
		message = "Ressource not found"
	}

	SendError(w, message, http.StatusNotFound)
}

// SendUnauthorized sends an error response with status unauthorized
func SendUnauthorized(w http.ResponseWriter, message interface{}) {
	if message == nil {
		message = "Not authorized"
	}

	SendError(w, message, http.StatusUnauthorized)
}

func send(w http.ResponseWriter, data interface{}, ok bool, err *responseError) {
	res := &response{
		Data: data,
		Ok:   ok,
		Err:  err,
	}
	json.NewEncoder(w).Encode(res)
}
